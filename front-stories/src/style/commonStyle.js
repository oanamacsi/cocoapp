import styled from "styled-components";

export const Text = styled.p`
  font-size: 20px;
  color: black;
`;
export const Labels = styled.h2`
 display:flex;
 justify-content:center
 font-family:Times New Roman;`;

 export const FormLabel = styled.label`
  display: flex;
  flex-direction: column;
`;
export const FormInput = styled.input`
height: 30px;
`;
export const Button = styled.input`
  width: 80px;
  height: 30px;
  background-color: #086751;
  color: white;
  font-weight: bold;
  font-size: 14px;
  margin-top: 0.5em;
  border-radius: 0.5em;
`;
export const WrappCenter = styled.div`
display: flex;
justify-content: center;
`;
export const ModalContainer = styled.form`
display: flex;
flex-direction: column;
width: 80%;
`;
export const WrappButtons = styled.div`
display: flex;
justify-content: space-around;
`;
export const WrappLabelImage = styled.div`
  display: flex;
  justify-content: space-between;
  padding-top:0.5em;
`;
export const Box = styled.div`
  width: 200px;
  height: 200px;
  border-radius: 1em;
  background-color: #dfe3e6;
  margin-top: 35px;
  :hover {
    background-color: light-grey;
    opacity: 0.5;
  }
`;