// import React, { Fragment } from 'react'
// import styled from 'styled-components'
// import { lifecycle, compose, withState, withHandlers } from 'recompose'
// import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'
// import { Labels } from '../style/commonStyle'
// import { Cards } from './'
// const statuses = ['To Do', 'In Progress', 'QA', 'Done']

// const DroppableColumn = ({ stories = [], searched,onDragEnd }) => {
//   onDragEnd = result => {
//     const { destination, source } = result
//     if (!destination) {
//       return
//     }}
//   return (
//     <DragDropContext onDragEnd={onDragEnd}>
//      <Droppable droppableId="droppable">
//           {(provided, snapshot) => (  
//           <div
//               ref={provided.innerRef}>
//       {statuses.map((status, index) => (
        
//         <div key={index}>
//           <Labels>{status}</Labels>
//           <div style={{ backgroundColor: 'grey' }}>
//             <Cards
//               filteredStories={stories
//                 .filter(story => story.status === status)
//                 .filter(story => story.title.includes(searched))}
//             />
//           </div>
//     </div> 
//   ))}
//   </div>
//   )}
//   </Droppable>


// </DragDropContext>
//   )
// }
// const enhance = compose(
//   withState('searched', 'setSearchedValue', ''),
//   withHandlers({
//     onChange: props => event => {
//       props.setSearchedValue(event.target.value)
//     },
//   }),
//   lifecycle({
//     componentDidMount() {
//       fetch('/story', {
//         method: 'GET',
//         headers: {
//           'Content-Type': 'application/json',
//           Accept: 'application/json',
//         },
//       })
//         .then(response => response.json())
//         .then(stories => this.setState({ stories }))
//         .catch(err => console.log(err))
//     },
//   }),
// )

// export default enhance(DroppableColumn)
// const AlignBox = styled.div`
//   display: grid;
//   grid-template-columns: 25% 25% 25% 25%;
//   justify-content: space-around;
//   margin-top: 70px;
// `
