import React, { Fragment } from 'react'
import { withState, withHandlers, compose, withStateHandlers } from 'recompose'
import styled from 'styled-components'
import Modal from 'react-modal'

import plusImage from '../images/plusImage.png'
import {
  Text,
  Labels,
  FormLabel,
  FormInput,
  Button,
  WrappCenter,
  ModalContainer,
  WrappButtons,
} from '../style/commonStyle'
const StoryModal = ({
  addStory,
  onChange,
  openModal,
  handleClick,
  card: { title, description, status, creator },
}) => {
  return (
    <Fragment>
      <WrappCenter>
        <Plus src={plusImage} onClick={handleClick} />
        <Text>Add a card</Text>
      </WrappCenter>
      <Modal
        isOpen={openModal}
        contentLabel="Create a new task"
        style={modalBox}
        ariaHideApp={false}
      >
        <Labels>Create a new task</Labels>
        <WrappCenter>
          <ModalContainer onSubmit={addStory}>
            <FormLabel>
              Story title:
              <FormInput
                type="text"
                name="title"
                placeholder="Add a title for your story"
                value={title}
                onChange={onChange}
              />
            </FormLabel>
            <FormLabel>
              Story description:
              <textarea
                type="text"
                value={description}
                name="description"
                placeholder="What is this story about"
                onChange={onChange}
              />
            </FormLabel>
            <FormLabel>
              Select a status:
              <select name="status" onChange={onChange}>
                <option value="To Do">To Do</option>
                <option value="In Progress">In Progress</option>
                <option value="QA">QA</option>
                <option value="Done">Done</option>
              </select>
            </FormLabel>
            <FormLabel>
              Who are you?
              <FormInput
                value={creator}
                type="text"
                name="creator"
                placeholder="What's your name?"
                onChange={onChange}
              />
            </FormLabel>
            <WrappButtons>
              <Button type="submit" onClick={handleClick} value="Close" />
              <Button type="submit" onSubmit={addStory} value="Submit" />
            </WrappButtons>
          </ModalContainer>
        </WrappCenter>
      </Modal>
    </Fragment>
  )
}

export default compose(
  withState('openModal', 'setOpened', false),
  withState('status', 'setStatus', 'To Do'),
  withStateHandlers(
    ({ title = '', description = '', status = '', creator = '' }) => ({
      card: {
        title,
        description,
        status,
        creator,
      },
    }),
    {
      onChange: ({ card }) => event => ({
        card: {
          ...card,
          [event.target.name]: event.target.value,
        },
      }),
    },
  ),
  withHandlers({
    handleClick: props => event => {
      props.setOpened(!props.openModal)
    },
    addStory: ({ card, setOpened }) => e => {
      // e.preventDefault()
      fetch('/story', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(card),
      })
        .then(r => r.json())
        .then(response => {
          console.log('s-a creat ', response)
          setOpened(false)
        })
    },
  }),
)(StoryModal)

const Plus = styled.img`
  width: 60px;
  height: 60px;
`
const modalBox = {
  content: {
    top: '200px',
    left: '440px',
    right: '350px',
    bottom: '80px',
    width: '500px',
    height: '300px',
  },
}
