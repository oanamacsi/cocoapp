import React, {Fragment} from 'react'
import {withState, withHandlers, compose, withStateHandlers,lifecycle} from 'recompose'

import {
  Button,
} from '../style/commonStyle'

const CardDelete=({
story,
deleteStory
})=>{
  return(
    <Fragment>
        <Button value="Delete" type="submit" onClick={deleteStory}/>
    </Fragment>
  )
}

export default compose(
  withHandlers({
    handleDeleteClick:props=>event=>{

    },
  deleteStory: ({story}) => event => {
  var storyId=story._id
    fetch('story/' +storyId, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
    window.location.reload()
  },
}),

)(CardDelete)