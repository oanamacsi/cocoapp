import React from "react";
import { compose } from "recompose";
import styled from "styled-components";

import StoryModal from "./Modal";
import { WrappLabelImage ,Text} from "../style/commonStyle";

const HeaderBar = ({ searched,onSearchChange }) => {
  return (
    <Header>
      <WrappLabelImage>
        <StoryModal />
        <WrappLabelImage>
      <Text>Search</Text>
      <SearchInput
        type="text"
        name="search"
        value={searched}
        placeholder="Search a title"
        onChange={onSearchChange}
      />
    </WrappLabelImage>
      </WrappLabelImage>
    </Header>
  );
};
export default compose(HeaderBar);

const Header = styled.div`
  height: 5em;
  background-color: #086751;
  width: 100%;
  position: -webkit-sticky;
  position: sticky;
  top: 0px;
`;
const SearchInput = styled.input`
  width: 150px;
  height: 30px;
  border-radius: 0.5em;
  margin-top:15px;
  margin-right:20px;
  margin-left:10px;
`;
