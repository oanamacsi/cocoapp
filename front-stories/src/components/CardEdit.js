import React, { Fragment } from 'react'
import Modal from 'react-modal'
import { withState, withHandlers, compose, withStateHandlers } from 'recompose'

import {
  Labels,
  Button,
  FormLabel,
  FormInput,
  WrappCenter,
  WrappButtons,
  ModalContainer,
} from '../style/commonStyle'

const CardEdit = ({
  story,
  isOpen,
  onChange,
  editStory,
  toggleModal,
}) => {
  return (
    <Fragment>
      <WrappCenter>
        <Button value="Edit" type="submit" onClick={toggleModal} />
      </WrappCenter>
      <Modal
        isOpen={isOpen}
        style={modalBox}
        ariaHideApp={false}
        contentLabel="Create a new task"
      >
        <Labels>Edit this story</Labels>
        <WrappCenter>
          <ModalContainer onSubmit={editStory}>
            <FormLabel>
              Story title:
              <FormInput
                type="text"
                name="title"
                defaultValue={story.title}
                onChange={onChange}
              />
            </FormLabel>
            <FormLabel>
              Story description:
              <textarea
                type="text"
                defaultValue={story.description}
                name="description"
                onChange={onChange}
              />
            </FormLabel>
            <FormLabel>
              Who are you?
              <FormInput
                defaultValue={story.creator}
                type="text"
                name="creator"
                onChange={onChange}
              />
            </FormLabel>
            <WrappButtons>
              <Button type="submit" onClick={toggleModal} value="Close" />
              <Button type="submit" onSubmit={editStory} value="Submit" />
            </WrappButtons>
          </ModalContainer>
        </WrappCenter>
      </Modal>
    </Fragment>
  )
}

export default compose(
  withState('isOpen', 'setModalVisibility', false),
  withStateHandlers(({ story }) => ({ story }), {
    onChange: ({ story }) => event => ({
      story: {
        ...story,
        [event.target.name]: event.target.value,
      },
    }),
  }),
  withHandlers({
    toggleModal: ({ setModalVisibility }) => () => {
      setModalVisibility(theCurrentState => !theCurrentState)
    },
    editStory: ({ story: { _id, ...story } }) => event => {
      // console.log('edit',story.story)
      fetch(`/story/${_id}`, {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ ...story }),
      })
    },
  }),
)(CardEdit)

const modalBox = {
  content: {
    top: '200px',
    left: '440px',
    right: '350px',
    bottom: '80px',
    width: '500px',
    height: '300px',
  },
}
