import React from 'react'
import styled from 'styled-components'
import {
  lifecycle,
  compose,
  withState,
  withHandlers,
  withStateHandlers,
} from 'recompose'
import Modal from 'react-modal'
import {
  Labels,
  WrappCenter,
  ModalContainer,
  FormLabel,
  FormInput,
  Button,
  WrappButtons,
  Box,
} from '../style/commonStyle'
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'
import { CardEdit } from './'
import {CardDelete} from './'

const Card = ({
  story,
  openModal,
  filteredStories,
  addComment,
  onChange,
  handleClick,
  comments = [],
  handleSubmitClick,
  updateStatus,
  comms: { name, comment, status },
  onDragEnd
}) =>  { 
  const move = (source, destination, droppableSource, droppableDestination) => {
    const sourceClone = Array.from(source)
    const destClone = Array.from(destination)
    const [removed] = sourceClone.splice(droppableSource.index, 1)

    destClone.splice(droppableDestination.index, 0, removed)

    const result = {}
    result[droppableSource.droppableId] = sourceClone
    result[droppableDestination.droppableId] = destClone

    return result
  }
  onDragEnd = result => {
    const { destination, source } = result
    if (!destination) {
      return
    }
    if (source.droppableId !== destination.droppableId) {
      // console.log('sourceList', sourceList)
      // console.log('destinationList', destinationList)
      // console.log('source', source)
      // console.log('destination', destination)

      // const result = move(sourceList, destinationList, source, destination)
      // updateSourceList(result.droppable)
      // updateDestinationList(result.droppable2)
    }
  }
  return (
  
  <WrappBox>
    {filteredStories.map((story,index) => (
         <Draggable key={story._id} draggableId={story._id} index={index}>
           {(provided, snapshot) => (
      <div key={story._id}  ref={provided.innerRef}
      {...provided.draggableProps}
      {...provided.dragHandleProps}>
        <Box onClick={handleSubmitClick(story)}>
          <TitleStory>{story.title}</TitleStory>
          <DescriptionStory>{story.description}</DescriptionStory>
          <CreatorStory>{story.creator}</CreatorStory>
        </Box>
        <WrappButtons>
        <CardEdit story={story} />
        <CardDelete story={story}/>
        </WrappButtons>
      </div>
           )}
      </Draggable>
    ))}


    <WrappButtons>
      <Modal
        isOpen={openModal}
        contentLabel="Create a new task"
        ariaHideApp={false}
      >
        <ChangeStatus>
          <FormLabel>
            Change status:
            <select name="status" onChange={onChange}>
              <option value="To Do">To Do</option>
              <option value="In Progress">In Progress</option>
              <option value="QA">QA</option>
              <option value="Done">Done</option>
            </select>
          </FormLabel>
          <Button onClick={updateStatus} type="submit" value="Change" />
        </ChangeStatus>
        <Labels>Comments</Labels>
        <WrappCenter>
          <ModalContainer>
            <FormLabel>
              What's your name?
              <FormInput
                type="text"
                name="name"
                placeholder="What's your name?"
                onChange={onChange}
              />
            </FormLabel>
            <FormLabel>
              Write your comment
              <textarea
                type="text"
                name="comment"
                placeholder="Write your comment"
                onChange={onChange}
              />
            </FormLabel>
            <WrappButtons>
              <Button type="submit" onClick={handleClick} value="Close" />
              <Button type="submit" onClick={addComment} value="Submit" />
            </WrappButtons>
          </ModalContainer>
        </WrappCenter>
        {comments
          .filter(comment => comment.storyId === story._id)
          .map((comment, index) => (
            <CommBox key={index}>
              <TitleStory>{comment.name}</TitleStory>
              <DescriptionStory>{comment.comment}</DescriptionStory>
            </CommBox>
          ))}
      </Modal>
    </WrappButtons>
  </WrappBox>
)
          }

const enhance = compose(
  withState('openModal', 'setOpened', false),
  withState('createCard', 'displayCard', true),
  withState('addComment', 'displayComment', ''),
  withState('story', 'displayStory', ''),
  withState('status', 'setStatus', 'To Do'),
  withState('updateStatus', 'setUpdatedStatus', ''),
  withStateHandlers(
    ({ name = '', comment = '', status = '' }) => ({
      comms: {
        name,
        comment,
        status,
      },
    }),
    {
      onChange: ({ comms }) => event => ({
        comms: {
          ...comms,
          [event.target.name]: event.target.value,
        },
      }),
    },
  ),
  withHandlers({
    handleClick: props => event => {
      props.setOpened(!props.openModal)
    },
    handleSubmitClick: props => story => event => {
      props.displayStory(story)
      props.setOpened(!props.openModal)
    },
    addComment: ({ comms, setOpened, story }) => event => {
      // event.preventDefault()
      var storyId = story._id
      fetch('/comments', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ ...comms, storyId }),
      })
        .then(r => r.json())
        .then(response => {
          console.log('s-a creat ', response)
          setOpened(false)
        })
    },
    updateStatus: story => event => {
      var storyId = story.story._id
      var status = story.comms.status
      fetch('/story/' + storyId, {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ ...story, status }),
      })
    },
    onDeleteStory: (state, props) => event => {
      props.handleClose()
      fetch('stories/' + props.id, {
        method: 'DELETE',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      })
    },
  }),
  lifecycle({
    componentDidMount() {
      fetch('/story', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      })
        .then(response => response.json())
        .then(stories => this.setState({ stories }))
        .catch(err => console.log(err))
      fetch('/comments/', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      })
        .then(response => response.json())
        .then(comments => this.setState({ comments }))
        .catch(err => console.log(err))
    },
  }),
)
export default enhance(Card)

const ChangeStatus = styled.div`
  width: 120px;
`
const TitleStory = styled.div`
  display: flex;
  flex-direction: column;
  text-align: center;
  padding-top: 20px;
  font-size: 25px;
  font-family: Times New Roman;
  color: black;
  margin-bottom: 10px;
`
const DescriptionStory = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 16px;
  color: black;
  margin-left: 40px;
  margin-right: 40px;
  font-family: Times New Roman;
`
const CreatorStory = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 50px;
  margin-right: 20px;
`
const WrappBox = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-top: 70px;
  margin-left: 30px;
`

const CommBox = styled.div`
  width: 800px;
  height: 120px;
  border-radius: 0.5em;
  background-color: #dfe3e6;
  margin-top: 30px;
  margin-left: 100px;
`
