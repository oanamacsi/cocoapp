import React, { Fragment } from 'react'
import ApolloClient from "apollo-boost";
import styled from 'styled-components'
import {
  lifecycle,
  compose,
  withState,
  withHandlers,
  withPropsOnChange,
} from 'recompose'
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'

import { Header, Cards } from './components'
import { Labels } from './style/commonStyle'

const client = new ApolloClient({
  uri: "https://vm8mjvrnv3.lp.gql.zone/graphql"
});
const statuses = ['To Do', 'In Progress', 'QA', 'Done']
const App = ({
  stories = [],
  searched,
  onChange,
  onDragEnd,
  source,
  destination,
  sourceList,
  destinationList,
  updateDestinationList,
  updateSourceList,
}) => {
  const move = (source, destination, droppableSource, droppableDestination) => {
    const sourceClone = Array.from(source)
 
    const destClone = Array.from(destination)
    const [removed] = sourceClone.splice(droppableSource.index, 1)

    destClone.splice(droppableDestination.index, 0, removed)

    const result = {}
    result[droppableSource.droppableId] = sourceClone
    result[droppableDestination.droppableId] = destClone

    return result
  }

  onDragEnd = result => {
    const { destination, source } = result
    if (!destination) {
      return
    }
    if (source.droppableId !== destination.droppableId) {
      console.log('source', source)
      console.log('destination', destination)

      const result = move(sourceList, destinationList, source, destination)
      updateSourceList(result.droppable1)
      updateDestinationList(result.droppable2)
      console.log('sourceList', sourceList)
      console.log('destinationList', destinationList)
    }
  }

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Header onSearchChange={onChange} />
      <AlignBox>
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <Labels>To Do</Labels>
          <Droppable droppableId="droppable1">
            {(provided, snapshot) => (
              <div ref={provided.innerRef}>
                <Cards
                  filteredStories={stories
                    .filter(story => story.status === 'To Do')
                    .filter(story => story.title.includes(searched))}
                />
              </div>
            )}
          </Droppable>
        </div>
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <Labels>In Progress</Labels>
          <Droppable droppableId="droppable2">
            {(provided, snapshot) => (
              <div ref={provided.innerRef}>
                <Cards
                  filteredStories={stories
                    .filter(story => story.status === 'In Progress')
                    .filter(story => story.title.includes(searched))}
                />
              </div>
            )}
          </Droppable>
        </div>
        <div style={{display:'flex',flexDirection: 'column',}}>
        <Labels>QA</Labels>
        <Droppable droppableId="droppable3">
          {(provided, snapshot) => (
            <div ref={provided.innerRef}>
              <Cards
                filteredStories={stories
                  .filter(story => story.status === 'QA')
                  .filter(story => story.title.includes(searched))}
              />
            </div>
          )}
        </Droppable>
        </div>
        <div style={{display:'flex',flexDirection: 'column',}}>
        <Labels>Done</Labels>
        <Droppable droppableId="droppable4">
          {(provided, snapshot) => (
            <div ref={provided.innerRef}>
              <Cards
                filteredStories={stories
                  .filter(story => story.status === 'Done')
                  .filter(story => story.title.includes(searched))}
              />
            </div>
          )}
        </Droppable>
        </div>
      </AlignBox>
    </DragDropContext>
  )
}
const enhance = compose(
  withState('searched', 'setSearchedValue', ''),
  withState('sourceList', 'updateSourceList', ''),
  withState('destinationList', 'updateDestinationList', ''),
  withHandlers({
    onChange: props => event => {
      props.setSearchedValue(event.target.value)
    },
  }),
  lifecycle({
    componentDidMount() {
      fetch('/story', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      })
        .then(response => response.json())
        .then(stories => this.setState({ stories }))
        .catch(err => console.log(err))
      fetch('/story', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      })
        .then(response => response.json())
        .then(item =>statuses.map((status,index)=>(item.filter(story=>story.status===status))) )
        // .then(item => console.log("aici",item))
      .then(this.props.updateSourceList)
      // .then(this.props.updateDestinationList)
    },
  }),
)

export default enhance(App)

const AlignBox = styled.div`
  display: grid;
  grid-template-columns: 25% 25% 25% 25%;
  justify-content: space-around;
  margin-top: 70px;
`
