const express = require("express");
const bodyParser = require("body-parser");
const MongoClient = require("mongodb").MongoClient;
var ObjectId = require("mongodb").ObjectID;
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var db;

MongoClient.connect(
  "mongodb://oana97:oana97@ds263571.mlab.com:63571/coco-stories",
  { useNewUrlParser: true },
  (err, client) => {
    if (err) return console.log(err);
    db = client.db("coco-stories");
    app.listen(3000, () => {
      console.log("listening on 3000");
    });
  }
);

app.get("/story", (req, res) => {
  db.collection("story")
    .find()
    .toArray((err, results) => {
      if (err) return console.error(err);
      res.send(results);
    });
});
app.post("/story", (req, res) => {
  db.collection("story").insertOne(req.body, (err, result) => {
    if (err) return console.log(err);
    res.send(result.ops[0]);
  });
});

app.get("/comments", (req, res) => {
  db.collection("comments")
    .find()
    .toArray((err, results)=> {
      if (err) return console.error(err);
      res.send(results);
    });
});
app.post("/comments", (req, res) => {
  db.collection("comments").insertOne(req.body, (err, result) => {
    if (err) return console.log(err);
    res.send(result.ops[0]);
  });
});

app.put("/story/:storyId", (req, res) => {
  const storyId = req.params.storyId;
  db.collection("story").findOneAndUpdate(
    { _id: ObjectId(storyId) },
    { $set: req.body },
    { returnOriginal: false },
    (err, result) => {
      if (err) {
        return console.log(err);
      } else {
        res.send(result.value);
      }
    }
  );
});

app.delete('/story/:storyId', (req, res) => {
  const storyId = req.params.storyId
  db.collection("story")
    .deleteOne({ _id: ObjectId(storyId) },(err,result)=>{
      if (err) {
        return console.log(err);
      } else {
        res.send('isDeleted');
      }
    })
    // .then(() => res.send('Deleted'))
    // .catch(err => console.error(err))
})
